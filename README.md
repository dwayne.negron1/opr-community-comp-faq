# OPR Competitive Rules FAQ

This document has been written to help differentiate the as-written rules of OPR from common house-rules and misconceptions.
All interpretations are for OPR v 2.50.

# Glossary
- **2D** - Two Dimension. A flat plane that ignores height.
- **LOS** - Line of Sight. A 2D measurement from one unit to another.
- **Model** - An individual figurine within a unit.
- **Unit** - A group of models that represents a logistical grouping in the game. A unit has X models to start, where X is the number in square brackets in the unit's name (ie: Warriors[3])

# General Rules
## Pre Game Clarifications
- Which terrain counts as cover? Difficult?
    - Before the game begins, you and your opponent(s) should agree on which cover keywords affect each piece of terrain. In the core rules, there are 5 keywords:
        - Impassible, Elevation, Cover, Difficult, Dangerous
    - A single piece of terrain can have as many keywords as makes sense, and all affects will apply.
    - A piece of terrain with no keywords is considered Open Terrain (the default with no special rules)

## Cover / LOS
- When does a unit gain cover?
    - A unit recieves cover when drawing LOS from the active unit, the model is in or behind a piece of terrain with the Cover keyword
    - If a unit can draw 2D LOS to either **ANY** part of a model base (single model) or **MORE THEN HALF** of the model bases (multi-model) in the unit without interesecting cover terrain the unit does not have cover.

- Does LOS contribute to cover?
    - No LOS does not contribute or provide cover it only removes

- Do enemy or allied units provide cover?
    - All units (enemy or allies) block LOS, but do not grant cover.

## Elevation
- How does elevation affect LOS?
    - Elevation is considered binary. Either a feature is elevated, or it is not. Units on different Elevations have no advantage or disadvantage when drawing LOS to each other. In the core game, Elevation is considered a movement tax only.

- Elevation and movement
    - Moving up or down Elevation terrain costs a movement penalty (ie -3" or -6") which is determined at the beginning of the game when the Elevation keyword is added to the terrain.

# Keywords
## Aegis
- How does Aegis stack with Regeneration?
    - Make two rolls. One as a normal Regeneration, and one as the Aegis specific roll.

## Aircraft
- How do aircraft interact with terrain and units on the board?
    - Aircraft do not interact with the board for movement. If the aircraft cannot move onto a spot due to terrain or units you can use a token to represent it. Measurements for future movement still must use the base as normal.

- How does LOS work on aircraft?
    - LOS on aircraft works as normal and must respect 2D, Terrain, and Units.

## Blast
- When do you determine Blast?
    - At decleration you count the amount of models in the targetted unit. If they are greater or equal to the blast number you get that many extra hits per a successful hits.
    - If the targetted unit has less models then your blast number at decleration you get that model counts extra hits instead.
    - The total number of hits from Blast is limited by the number of models in the target unit.

- Can Blast wound rolls be reduced by units model count?
    - No blast only affects the amount of hits that leads to wound rolls

## Impact
- I have a unit with 5 models. Those 5 models each have 2 weapon profiles - an A1, and an A2 weapon. I give them Impact. How many hits do they gain when charging?
    - 5 hits. Impact is per model (and therefore reduces as the model count reduces), and does not take on any specific weapon profile (no AP or special rules)

## Poison
- An A3 weapon with Poison attacks and rolls three sixes. How many hits does it apply?
    - 9 hits. 3x3.

- The same weapon now has deadly. How many hits now?
    - 9 hits. Each wound causes 3 wounds. Yes, you could see 27 wounds.    

## Transport
- Where do you measure for models leaving a transport?
    - You place models base to base with the transport and then measure 6" move as normal.

# Faction Specific Rules
## Tao
- Are drones an extra unit?
    - Sometimes. There are Drone units which do have their own independent activation. There are also drone upgrades for units. Drone upgrades do not get their own activation. A drone model can be added attached to the unit, but it is regarded as invisible (ie: no LOS can be drawn to or from it, it has no benefits of cover, etc)

- Are Spotting lasers affected by attack penalties / cover / Good Shot / etc?
    - No. Spotting Lasers are always successful on a roll of 4+.

- I have a unit with 5 spotting lasers. Can I target 5 different units with my lasers?
    - Yes. Spotting Lasers are not a weapon profile, so split it up as desired.

- What is the difference between Spotting Lasers and Drones (Spotting Lasers)?
    - None, both are considered unit upgrades that provide the Spotting Lasers special rule to a model.


